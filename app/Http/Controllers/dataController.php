<?php

namespace App\Http\Controllers;
use App\User;
use DB;
use Request;

class dataController extends Controller
{

    public function makeAdministratorWeb($id){
        $update=User::where('id', $id)
            ->update(['userType'=>'Administrator']);

        if($update){
            return json_encode(['response'=>'true']);
        }
        else{
            return json_encode(['response'=>'false']);
        }
    }
    public function makeDataClerkWeb($id){
        $update=User::where('id', $id)
            ->update(['userType'=>'Data clerk']);

        if($update){
            return json_encode(['response'=>'true']);
        }
        else{
            return json_encode(['response'=>'false']);
        }
    }
    public function makeBasicUserWeb($id){
        $update=User::where('id', $id)
            ->update(['userType'=>'Basic user']);

        if($update){
            return json_encode(['response'=>'true']);
        }
        else{
            return json_encode(['response'=>'false']);
        }

    }










    public function makeAdministrator($id){
        $update=User::where('id', $id)
            ->update(['userType'=>'Administrator']);

        if($update){
            return json_encode(['response'=>'true']);
        }
        else{
            return json_encode(['response'=>'false']);
        }
    }
    public function makeDataClerk($id){
        $update=User::where('id', $id)
            ->update(['userType'=>'Data clerk']);

        if($update){
            return json_encode(['response'=>'true']);
        }
        else{
            return json_encode(['response'=>'false']);
        }
    }
    public function makeBasicUser($id){
        $update=User::where('id', $id)
            ->update(['userType'=>'Basic user']);

        if($update){
            return json_encode(['response'=>'true']);
        }
        else{
            return json_encode(['response'=>'false']);
        }

    }
    public function userHome(){

        return view('users.home');
    }
    public function getUser($id){
        $userData=User::where('id',$id)->select('firstName','lastName','phoneNumber','email','address','coordinates','id')->get();
        return json_encode($userData);
    }
    public function read(){
        $userData=DB::table('users')->select('firstName','lastName','phoneNumber','email','address','coordinates','id','userType')->get();
        return json_encode($userData);
    }
    public function update($id){
        $firstName= Request::get('firstName');
        $lastName=Request::get('lastName');
        $email=Request::get('email');
        $phoneNumber=Request::get('phoneNumber');
        $userType=Request::get('userType');
        $address=Request::get('address');
        $coordinates=Request::get('coordinates');
        $id=User::where('email',$email)->value('id');
        $update=User::where('id', $id)
            ->update(['firstName' => $firstName,'lastName'=>$lastName,'email'=>$email,'phoneNumber'=>$phoneNumber,'userType'=>$userType,'address'=>$address,'coordinates'=>$coordinates]);

        if($update){
            return json_encode(['response'=>'true']);
        }
        else{
            return json_encode(['response'=>'false']);
        }
    }


    public function updateWeb(){
        $id=Request::get('id');
        $firstName= Request::get('firstName');
        $lastName=Request::get('lastName');
        $email=Request::get('email');
        $phoneNumber=Request::get('phoneNumber');
        $userType=Request::get('userType');
        $address=Request::get('address');
        $coordinates=Request::get('coordinates');
        $id=User::where('email',$email)->value('id');
        $update=User::where('id', $id)
            ->update(['firstName' => $firstName,'lastName'=>$lastName,'email'=>$email,'phoneNumber'=>$phoneNumber,'userType'=>$userType,'address'=>$address,'coordinates'=>$coordinates]);

        if($update){
            return json_encode(['response'=>'true']);
        }
        else{
            return json_encode(['response'=>'false']);
        }
    }



    public function delete($id){
        $delete = User::where('id', $id)->delete();
        if($delete){
            return ('true');
        }
        else{
            return ('false');
        }







    }

    //
}
