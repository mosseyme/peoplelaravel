<?php

namespace App\Http\Controllers;
use Request;
use App\User;
use Auth;
use DB;
class authenticationController extends Controller
{
    public function APIAuthentication(){
        $email= Request::get('email');
        $password=Request::get('password');
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $userData=DB::table('users')->where('email',$email)->select('id','firstName','lastName','phoneNumber','email','address','coordinates','userType')->first();
            return json_encode($userData);

        }
        else {
            return 'false';

        }
    }
    public function registration(){
        $firstName= Request::get('firstName');
        $lastName=Request::get('lastName');
        $email=Request::get('email');
        $password=bcrypt(Request::get('password'));
        $phoneNumber=Request::get('phoneNumber');
        $userType=Request::get('userType');
        $address=Request::get('address');
        $coordinates=Request::get('coordinates');

        $emailCheck=User::where('email',$email)->first();
        if($emailCheck==null){
            $User=new User();
            $User->firstName=$firstName;
            $User->lastName=$lastName;
            $User->email=$email;
            $User->password=$password;
            $User->phoneNumber=$phoneNumber;
            $User->userType=$userType;
            $User->address=$address;
            $User->coordinates=$coordinates;
            $User->save();


            return json_encode(['responsee'=>'true']);

        }
        else{
            return json_encode(['responsee'=>'false']);
        }


    }

    //
}
