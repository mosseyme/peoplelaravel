@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Hi {{Auth::User()->firstName}} {{Auth::User()->LastName}}
                </div>
                <div class="panel-body">
                    <p>
                        Your user role is  {{Auth::User()->userType}}. <br>
                        Basic users can't edit anything on the web app. <br>Data Clerks can view all records. <br>Administrators can alter user roles.
                    </p>
                </div>

            </div>
        </div>

    </div>
    @if(Auth::user()->userType=='Administrator'||Auth::user()->userType=='Data clerk')
        <div class="container">
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <td>
                                ID
                            </td>
                            <td>
                                First Name

                            </td>
                            <td>
                                Last Name
                            </td>
                            <td>
                                Email
                            </td>
                            <td>
                                Phone
                            </td>
                            <td>
                                Address
                            </td>
                            <td>
                                Coordinates
                            </td>

                        </tr>

                        </thead>
                        <tbody>
                        @foreach($users as $users)

                                <tr>
                                    <td>
                                        {{$users->id}}

                                    </td>
                                    <td>
                                        <input type="hidden" class="form-control" value="{{$users->id}}" name="id">
                                        <input type="text" class="form-control" value="{{$users->firstName}}" name="firstName">


                                    </td>
                                    <td>
                                        <input type="text" class="form-control" value="{{$users->lastName}}" name="lastName">



                                    </td>
                                    <td>
                                        <input type="email" class="form-control" value="{{$users->email}}" name="email">


                                    </td>
                                    <td>
                                        <input type="text" class="form-control" value="{{$users->phoneNumber}}" name="phoneNumber">


                                    </td>
                                    <td>
                                        <input type="text" class="form-control" value="{{$users->address}}" name="address">

                                    </td>
                                    <td><input type="text" class="form-control" name="coordinates" value="{{$users->coordinates}}">

                                    </td>

                                    @if(Auth::user()->userType=='Administrator')
                                    <td>
                                        <a href="users/update/basicUser/{{$users->id}}" class="button btn btn-secondary"> Make basic user</a>

                                    </td>
                                    <td>
                                        <a href="users/update/dataClerk/{{$users->id}}" class="button btn btn-secondary"> Make Data clerk</a>

                                    </td>
                                    <td>
                                        <a href="users/update/administrator/{{$users->id}}" class="button btn btn-secondary"> Make Administrator user</a>

                                    </td>
                                    @endif
                                </tr>

                        @endforeach
                        </tbody>
                    </table>



                </div>
            </div>
        </div>
    </div>
</div>
    @endif



@endsection
