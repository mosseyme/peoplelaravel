<?php
header('Access-Control-Allow-Origin: *');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('authenticate','authenticationController@APIAuthentication');

Route::post('registration','authenticationController@registration');
Route::get('users/{id}','dataController@getUser');
Route::post('/users/update/{id}', 'dataController@update');

Route::post('/users/update/administrator/{id}', 'dataController@makeAdministrator');
Route::post('/users/update/basicUser/{id}', 'dataController@makeBasicUser');
Route::post('/users/update/dataClerk/{id}', 'dataController@makeDataClerk');







