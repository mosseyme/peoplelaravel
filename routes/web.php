<?php
header('Access-Control-Allow-Origin: *');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('login',function (){
    return view('login');
});

Route::post('login','authenticationController@webAuthentication');

Route::get('home', 'dataController@userHome')->middleware('auth');
Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/read', 'dataController@read');
Route::post('/update/{id}', 'dataController@update');
Route::post('/delete/{id}', 'dataController@delete');



Route::get('/users/update/administrator/{id}', 'dataController@makeAdministratorWeb');
Route::get('/users/update/basicUser/{id}', 'dataController@makeBasicUserWeb');
Route::get('/users/update/dataClerk/{id}', 'dataController@makeDataClerkWeb');
Route::post('/users/update/web', 'dataController@updateWeb');